const add = require('./sketch');
describe('add', () => {
  it('should add two numbers', () => {
    expect(add(3, 2)).toBe(5);
  });
});